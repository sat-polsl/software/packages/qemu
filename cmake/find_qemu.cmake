function(_qemu_version)
    execute_process(
        COMMAND ${QEMU_EXECUTABLE} --version
        OUTPUT_VARIABLE full_qemu_version
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    message(TRACE "QEMU version full: ${full_qemu_version}")
    
    string(REGEX MATCH "[0-9]+\.[0-9]+\.[0-9]+" qemu_version "${full_qemu_version}")

    message(DEBUG "QEMU version: ${qemu_version}")
    set(QEMU_VERSION ${qemu_version} PARENT_SCOPE)
endfunction()

function(find_qemu VERSION)
    if(QEMU_FOUND)
        return()
    endif()

    set(OPTIONS REQUIRED NO_SYSTEM_PATH)
    set(ONE_VALUE)
    set(MULTI_VALUE)
    cmake_parse_arguments(PARSE_ARGV 1 FIND_QEMU "${OPTIONS}" "${ONE_VALUE}" "${MULTI_VALUE}")

    if (${CMAKE_HOST_SYSTEM_NAME} STREQUAL Windows)
        set(EXE ".exe")
    endif ()

    if(FIND_QEMU_REQUIRED)
        set(REQUIRED "REQUIRED")
    endif()
    if(FIND_QEMU_NO_SYSTEM_PATH)
        set(NO_SYSTEM_PATH "NO_SYSTEM_ENVIRONMENT_PATH")
    endif()

    find_program(QEMU_EXECUTABLE NAMES qemu-system-arm${EXE} PATHS ${QEMU_ROOT} ENV{QEMU_ROOT} ${REQUIRED} ${NO_SYSTEM_PATH})

    if(QEMU_EXECUTABLE)
        _qemu_version()

        if(NOT VERSION VERSION_EQUAL QEMU_VERSION)
            if(NOT FIND_QEMU_REQUIRED)
                message(VERBOSE "Found QEMU: ${QEMU_EXECUTABLE} (found version \"${QEMU_VERSION}\", required \"${VERSION}\")")
                unset(QEMU_EXECUTABLE CACHE)
                set(QEMU_FOUND FALSE PARENT_SCOPE)
                return()
            endif()
            message(FATAL_ERROR "QEMU not found. (Required: ${VERSION}, found: ${QEMU_VERSION})")
        endif()

        message(STATUS "Found QEMU: ${QEMU_EXECUTABLE} (found version \"${QEMU_VERSION}\", required \"${VERSION}\")")
        set(QEMU_FOUND TRUE PARENT_SCOPE)
    else()
        message(DEBUG "QEMU not found")
        set(QEMU_FOUND FALSE PARENT_SCOPE)
    endif()
endfunction()
