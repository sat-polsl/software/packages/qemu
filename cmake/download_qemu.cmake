set(QEMU_WINDOWS_URL https://gitlab.com/api/v4/projects/33158668/jobs/5681767439/artifacts/qemu-system-arm-w64.zip)
set(QEMU_LINUX_URL https://gitlab.com/api/v4/projects/33158668/jobs/5681767416/artifacts/qemu-system-arm)
set(QEMU_DARWIN_ARM64_URL https://gitlab.com/api/v4/projects/33158668/jobs/5681767418/artifacts/qemu-system-arm)

macro(_download_for_darwin)
    execute_process(
        COMMAND uname -m
        OUTPUT_VARIABLE ARCHITECTURE
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    if (ARCHITECTURE STREQUAL "x86_64")
        message(FATAL_ERROR "We don't provide our custom QEMU builds for Darwin x86_64")
    elseif (ARCHITECTURE STREQUAL "arm64")
        set(QEMU_DARWIN_URL ${QEMU_DARWIN_ARM64_URL})
    endif()

    message(VERBOSE "Downloading QEMU for Darwin ${ARCHITECTURE} from: ${QEMU_DARWIN_URL}")

    file(DOWNLOAD ${QEMU_DARWIN_URL} ${ROOT}/tmp/qemu-system-arm)
    file(
        COPY ${ROOT}/tmp/qemu-system-arm
        DESTINATION ${ROOT}
        FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
        )
    file(REMOVE_RECURSE ${ROOT}/tmp)
endmacro()

macro(_download_for_linux)
    message(VERBOSE "Downloading QEMU for Linux from: ${QEMU_LINUX_URL}")

    file(DOWNLOAD ${QEMU_LINUX_URL} ${ROOT}/tmp/qemu-system-arm)
    file(
        COPY ${ROOT}/tmp/qemu-system-arm
        DESTINATION ${ROOT}
        FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
        )
    file(REMOVE_RECURSE ${ROOT}/tmp)
endmacro()

macro(_download_for_windows)
    message(VERBOSE "Downloading QEMU for Windows from: ${QEMU_WINDOWS_URL}")

    file(DOWNLOAD ${QEMU_WINDOWS_URL} ${ROOT}/qemu.zip)
    file(ARCHIVE_EXTRACT 
         INPUT ${ROOT}/qemu.zip
         DESTINATION ${ROOT})
    file(REMOVE ${ROOT}/qemu.zip)
endmacro()

function(_calculate_hash RESULT)
    message(DEBUG "Calculating hashes")
    
    file(GLOB FILES "${ROOT}/*")

    set(HASH_LIST "")
    foreach(FILE ${FILES})
        set(HASH_VALUE "")
        file(MD5 ${FILE} HASH_VALUE)
        list(APPEND HASH_LIST ${HASH_VALUE})

        message(DEBUG "Hashed ${FILE}: ${HASH_VALUE}")
    endforeach()

    message(DEBUG "Hashing result: ${HASH_LIST}")
    set(${RESULT} ${HASH_LIST} PARENT_SCOPE)
endfunction()

macro(download_qemu)
    if(NOT IS_DIRECTORY ${ROOT})
        file(MAKE_DIRECTORY ${ROOT})
    endif()

    if(WIN32)
        _download_for_windows()
    elseif(APPLE)
        _download_for_darwin()
    elseif(UNIX)
        _download_for_linux()
    endif()

    set(HASH "")
    _calculate_hash(HASH)

    file(WRITE ${ROOT}/.md5 "${HASH}")
endmacro()

